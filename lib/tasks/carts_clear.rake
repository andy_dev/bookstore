namespace :carts do
  task :clear => :environment do
    Cart.outdated.destroy_all
  end
end
