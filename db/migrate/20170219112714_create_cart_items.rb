class CreateCartItems < ActiveRecord::Migration[5.0]
  def change
    create_table :cart_items do |t|
      t.belongs_to :cart, foreign_key: true
      t.belongs_to :book, foreign_key: true
      t.decimal :price, precision: 10, scale: 2, null: false, default: 0.0
      t.integer :quantity, null: false, default: 0

      t.timestamps
    end
  end
end
