yaml = YAML.load_file('db/seeds/books.yml')

yaml.each do |book|
  Book.create!(name: book.last['name'], description: book.last['description'], price: book.last['price'])
end
