class CartDecorator < Draper::Decorator
  delegate_all

  def total
    h.number_to_currency(object.total)
  end
end
