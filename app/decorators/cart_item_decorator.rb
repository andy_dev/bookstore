class CartItemDecorator < Draper::Decorator
  delegate_all

  def name
    object.book.name
  end

  def description
    object.book.description
  end

  def price
    h.number_to_currency(object.price)
  end
end
