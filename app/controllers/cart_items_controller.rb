class CartItemsController < ApplicationController
  respond_to :js
  before_action :find_book, except: [:clear]

  def add
    @result = AddBookToCart.call(cart: current_cart, book: @book)
  end

  def remove
    RemoveBookFromCart.call(cart: current_cart, book: @book)
  end

  def set_quantity
    @result = SetBookQuantityInCart.call(cart: current_cart, book: @book, quantity: params[:quantity])
  end

  def clear
    ClearCart.call(cart: current_cart)
  end

  private

  def find_book
    @book = Book.find(params[:book_id])
  end
end
