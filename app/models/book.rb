class Book < ApplicationRecord
  scope :top, -> { limit(10) }
end
