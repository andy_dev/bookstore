class Cart < ApplicationRecord
  has_many :cart_items, dependent: :destroy

  scope :outdated, -> { where('updated_at < ?', 2.weeks.agoc) }
end
