class RemoveCartItem
  include Interactor

  def call
    context.cart.cart_items.find_by(book: context.book).destroy
  end
end
