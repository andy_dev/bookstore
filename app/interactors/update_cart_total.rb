class UpdateCartTotal
  include Interactor

  def call
    context.cart.update(total: context.cart.reload.cart_items.sum('price * quantity'))
  end
end
