class CreateCartItem
  include Interactor

  def call
    cart_item.assign_attributes(quantity: cart_item.quantity.next)
    cart_item_validator = CartItemQuantityValidator.new(cart_item)

    if cart_item_validator.valid?
      cart_item.save
    else
      context.fail! message: cart_item_validator.errors.full_messages.join(', ')
    end
  end

  private

  def cart_item
    @_cart_item ||= context.cart.cart_items
                      .create_with(price: context.book.price, quantity: 0)
                      .find_or_initialize_by(book: context.book)
  end
end
