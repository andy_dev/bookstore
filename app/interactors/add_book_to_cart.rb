class AddBookToCart
  include Interactor::Organizer

  organize [CreateCartItem, UpdateCartTotal]
end
