class RemoveAllCartItems
  include Interactor

  def call
    context.cart.cart_items.destroy_all
  end
end
