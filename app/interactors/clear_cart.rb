class ClearCart
  include Interactor::Organizer

  organize [RemoveAllCartItems, UpdateCartTotal]
end
