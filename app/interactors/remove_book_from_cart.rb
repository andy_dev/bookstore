class RemoveBookFromCart
  include Interactor::Organizer

  organize [RemoveCartItem, UpdateCartTotal]
end
