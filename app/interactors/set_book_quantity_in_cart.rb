class SetBookQuantityInCart
  include Interactor::Organizer

  organize [UpdateCartItemQuantity, UpdateCartTotal]
end
