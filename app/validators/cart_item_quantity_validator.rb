class CartItemQuantityValidator < SimpleDelegator
  include ActiveModel::Validations

  validates :quantity, numericality: { only_integer: true, greater_than_or_equal_to: 1, less_than_or_equal_to: 1000 }
end
