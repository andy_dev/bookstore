$(document).on 'turbolinks:load', ->
  $('.js-quantity-change').quantityChange()

$.fn.quantityChange = ->
  @each ->
    xhrPool = []

    $(@).on 'input', (e) ->
      $.each xhrPool, (idx, jqXHR) ->
        jqXHR.abort()
      quantity = parseInt($(@).val())
      if isNaN(quantity) || quantity < 1
        quantity = 1
        $(@).val(quantity)
      else if quantity > 1000
        quantity = 1000
        $(@).val(quantity)
      $.ajax
        url: Routes.set_quantity_cart_items_path()
        method: 'POST'
        dataType: 'script'
        data:
          book_id: $(@).data('id')
          quantity: quantity
        beforeSend: (jqXHR) ->
          xhrPool.push(jqXHR)
        complete: (jqXHR) ->
          xhrPool = $.grep xhrPool, (x) ->
            x != jqXHR
