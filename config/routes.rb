Rails.application.routes.draw do
  root to: 'books#index'
  resources :books, only: [:index]
  resources :carts, only: [:index], path: 'cart', as: 'cart'
  resources :cart_items, only: [] do
    collection do
      post 'add'
      post 'remove'
      post 'set_quantity'
      post 'clear'
    end
  end
end
