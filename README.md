# Bookstore

This is a simple test project that shows an example of "skinny controllers, skinny models" Rails project.

Project uses interactors to handle all "Cart" logic.

## Requirements

- Ruby >= 2.3.1
- nodejs or any other runtime library for compiling coffescript

## Getting started

First clone this repo using git:

```console
$ git clone git@gitlab.com:andy_dev/bookstore.git
```    

Then go into project directory and run `bundle install`

Next, you need to create the database and seed it with data:

```console
$ rake db:setup
```

That's it! Now you can start Rails server with `rails s` and navigate to `localhost:3000` to see the result.
