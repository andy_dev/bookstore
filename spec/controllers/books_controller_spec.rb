require 'rails_helper'

RSpec.describe BooksController, type: :controller do
  describe '#index' do
    it 'should render index template' do
      get :index
      expect(response).to have_http_status(:success)
      expect(response).to render_template(:index)
    end
  end
end
