require 'rails_helper'

RSpec.describe CartItemsController, type: :controller do
  let(:add_request) { -> { post :add, params: { book_id: Book.last.id, format: :js } } }

  describe '#add' do
    it 'should add book to the cart' do
      add_request.call
      expect(Cart.last.cart_items.first.book.id).to eq(Book.last.id)
    end

    it 'should increase number of books in cart if exists' do
      expect(add_request).to change(Cart, :count).by(1)
      expect(Cart.last.cart_items.last.quantity).to eq(1)
      expect(add_request).to change { Cart.last.cart_items.last.quantity }.by(1)
    end

    it 'should not allow to add more than 1000 books' do
      add_request.call
      Cart.last.cart_items.first.update(quantity: 1000)
      expect(add_request).not_to change { Cart.last.cart_items.last.quantity }
    end
  end

  describe '#remove' do
    before { 3.times { add_request.call } }

    it 'should completely remove cart item from cart' do
      remove_request = -> { post :remove, params: { book_id: Book.last.id, format: :js } }
      expect(remove_request).to change(Cart.last.cart_items, :count).by(-1)
    end
  end

  describe '#set_quantity' do
    before { add_request.call }

    it 'should set new quantity' do
      set_quantity_request = -> { post :set_quantity, params: { book_id: Book.last.id, quantity: 10, format: :js } }
      expect(set_quantity_request).to change { Cart.last.cart_items.last.quantity }.to(10)
    end

    it 'should not allow to change quantity to invalid value' do
      set_quantity_request = -> { post :set_quantity, params: { book_id: Book.last.id, quantity: 1001, format: :js } }
      expect(set_quantity_request).not_to change { Cart.last.cart_items.last.quantity }
    end
  end

  describe '#clear' do
    before do
      Book.first(3).each do |book|
        post :add, params: { book_id: book.id, format: :js }
      end
    end

    it 'should completely clear the cart' do
      clear_request = -> { post :clear, params: { format: :js } }
      expect(clear_request).to change(Cart.last.cart_items, :count).by(-3)
    end
  end
end
