require 'rails_helper'

RSpec.describe Cart, type: :model do
  let(:cart) { Cart.new }

  subject { cart }

  it 'should have required relations' do
    should have_many(:cart_items).dependent(:destroy)
  end
end
