require 'rails_helper'

RSpec.describe CartItem, type: :model do
  let(:cart_item) { CartItem.new }

  subject { cart_item }

  it 'should have required relations' do
    should belong_to :cart
    should belong_to :book
  end
end
