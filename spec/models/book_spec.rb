require 'rails_helper'

RSpec.describe Book, type: :model do
  let(:book) { Book.new }

  subject { book }

  it 'should have required scope' do
    expect(subject.class).to respond_to :top
  end
end
